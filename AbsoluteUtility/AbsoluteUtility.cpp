// AbsoluteUtility.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//

#include "pch.h"
#include <iostream>
#include <vector>
#include <numeric>

float maxDistanceGrenade = 50, maxDistancePistole = 70, maxDistanceFist = 2, 
maxHealth_me = 20, maxHealth_enemy = 100, 
maxAmmo_meGrenade = 3, maxAmmo_mePistol = 200, maxAmmo_enemy = 50;


struct Knowledge {
	float distance,
		health_me,
		health_enemy,
		ammo_meGrenade,
		ammo_mePistole,
		ammo_enemy;
};

float ConsiderationUtilityLinearDistanceGrenade(bool ascending, float wissen)
{
	if (wissen > maxDistanceGrenade)
		return 0;
	else if (ascending)
			return wissen / maxDistanceGrenade;
		else
			return 1 - (wissen / maxDistanceGrenade);
}
float ConsiderationUtilityLinearHealthMeGrenade(bool ascending, float wissen)
{
	if (ascending)
		return wissen / maxHealth_me;
	else
		return 1 - (wissen / maxHealth_me);
}
float ConsiderationUtilityQuadraticHealthEnemyGrenade(bool ascending, bool hillshape, float strength, float wissen)
{
	if (ascending)
		if (hillshape)
			return (pow((wissen / maxHealth_enemy), (1 / strength)));
		else
			return (pow((wissen / maxHealth_enemy), strength));
	else
		if (hillshape)
			return 1 - (pow((wissen / maxHealth_enemy), (1 / strength)));
		else
			return 1 - (pow((wissen / maxHealth_enemy), strength));
}
float ConsiderationUtilityLinearAmmoMeGrenade(bool ascending, float wissen)
{
	if (ascending)
		return wissen / maxAmmo_meGrenade;
	else
		return 1 - (wissen / maxAmmo_meGrenade);
}
float ConsiderationUtilityQuadraticAmmoEnemy(bool ascending, bool hillshape, float strength, float wissen)
{
	if (ascending)
		if (hillshape)
			return (pow((wissen / maxAmmo_enemy), (1/strength)));
		else
			return (pow((wissen / maxAmmo_enemy), strength));
	else
		if (hillshape)
			return 1 - (pow((wissen / maxAmmo_enemy), (1 / strength)));
		else
			return 1 - (pow((wissen / maxAmmo_enemy), strength));
}

float ConsiderationUtilitySCurveDistancePistole(bool ascending, bool ramp, float strength, float wissen)
{
	if (ascending)
		if (ramp)
			return 10*pow(wissen,3)-15*pow(wissen,4)+6*pow(wissen,5);
		else
			return 0;
	else
		if (ramp)
			return 0;
		else
			return 1-(10 * pow(wissen, 3) - 15 * pow(wissen, 4) + 6 * pow(wissen, 5));
}
float ConsiderationUtilityQuadraticAmmoMePistole(bool ascending, bool hillshape, float strength, float wissen)
{
	if (ascending)
		if (hillshape)
			return 0;
		else
			return (pow((wissen / maxAmmo_mePistol), strength));
	else
		if (hillshape)
			return 1 - (pow((wissen / maxAmmo_mePistol), strength));
		else
			return 0;
}

float ConsiderationUtilityDistanceFist(float wissen)
{
	if (wissen < 2)
		return 1;
	else
		return 0;
}
float ConsiderationUtilityQuadraticHealthMeFist(bool ascending, bool hillshape, float strength, float wissen)
{
	if (ascending)
		if (hillshape)
			return (pow((wissen / maxHealth_me), (1 / strength)));
		else
			return (pow((wissen / maxHealth_me), strength));
	else
		if (hillshape)
			return 1 - (pow((wissen / maxHealth_me), (1 / strength)));
		else
			return 1 - (pow((wissen / maxHealth_me), strength));
}
float ConsiderationUtilityLinearHealthEnemyFist(bool ascending, float wissen)
{
	if (ascending)
		return wissen / maxHealth_enemy;
	else
		return 1 - (wissen / maxHealth_enemy);
}

float ArithmeticalFU(std::vector<float> EU)
{
	float sum_of_elems=0;
	for(std::vector<float>::iterator it = EU.begin(); it!= EU.end(); ++it)
		sum_of_elems += *it;
	return sum_of_elems / EU.size();
}

int main()
{
	std::vector<float> GrenadeEU, PistoleEU, FistEU;
	Knowledge Wissen;
	//Inserting Values
    std::cout << "Insert Values: \n"; 
	std::cout << "Distance(50/70/2): ";
	std::cin >> Wissen.distance;
	std::cout << "My Health(max 20): ";
	std::cin >> Wissen.health_me;
	std::cout << "Enemy Health(max 100): ";
	std::cin >> Wissen.health_enemy;
	std::cout << "My Grenade Ammu(max 3): ";
	std::cin >> Wissen.ammo_meGrenade;
	std::cout << "My Pistole Ammu(max 200): ";
	std::cin >> Wissen.ammo_mePistole;
	std::cout << "Enemy Ammunition(max 50): ";
	std::cin >> Wissen.ammo_enemy;
	std::cout << std::endl;

	GrenadeEU.push_back(ConsiderationUtilityLinearDistanceGrenade(true, Wissen.distance));
	GrenadeEU.push_back(ConsiderationUtilityLinearHealthMeGrenade(true, Wissen.health_me));
	GrenadeEU.push_back(ConsiderationUtilityQuadraticHealthEnemyGrenade(true, false, 3, Wissen.health_enemy));
	GrenadeEU.push_back(ConsiderationUtilityLinearAmmoMeGrenade(true, Wissen.ammo_meGrenade));
	GrenadeEU.push_back(ConsiderationUtilityQuadraticAmmoEnemy(true, true, 2, Wissen.ammo_enemy));
	std::cout << "Distance-Grenade U: " << GrenadeEU[0] << std::endl;
	std::cout << "Health Me-Grenade U: " << GrenadeEU[1] << std::endl;
	std::cout << "Health Enemy-Grenade U: " << GrenadeEU[2] << std::endl;
	std::cout << "Ammo Me-Grenade U: " << GrenadeEU[3] << std::endl;
	std::cout << "Ammo Enemy-Grenade U: " << GrenadeEU[4] << std::endl;
	std::cout << "FU Grenade: " << ArithmeticalFU(GrenadeEU)<< std::endl;

	PistoleEU.push_back(ConsiderationUtilitySCurveDistancePistole(false, false, 0, (Wissen.distance/maxDistancePistole)));
	PistoleEU.push_back(ConsiderationUtilityQuadraticAmmoMePistole(true, false, 3, Wissen.ammo_mePistole));
	PistoleEU.push_back(ConsiderationUtilityQuadraticAmmoEnemy(false, false, 2, Wissen.ammo_enemy));
	std::cout << "\nDistance-Pistole U: " << PistoleEU[0] << std::endl;
	std::cout << "Health Me-Pistole U: irrelevant"  << std::endl;
	std::cout << "Health Enemy-Pistole U: irrelevant"  << std::endl;
	std::cout << "Ammo Me-Pistole U: " << PistoleEU[1] << std::endl;
	std::cout << "Ammo Enemy-Pistole U: " << PistoleEU[2] << std::endl;
	std::cout << "FU Pistole: " << ArithmeticalFU(PistoleEU) << std::endl;

	FistEU.push_back(ConsiderationUtilityDistanceFist(Wissen.distance));
	FistEU.push_back(ConsiderationUtilityQuadraticHealthMeFist(true, false, 3, Wissen.health_me));
	FistEU.push_back(ConsiderationUtilityLinearHealthEnemyFist(false, Wissen.health_enemy));
	FistEU.push_back(ConsiderationUtilityQuadraticAmmoEnemy(false, false, 2, Wissen.ammo_enemy));
	std::cout << "\nDistance-Fist U: " << FistEU[0] << std::endl;
	std::cout << "Health Me-Fist U: " << FistEU[1] << std::endl;
	std::cout << "Health Enemy-Fist U: " << FistEU[2] << std::endl;
	std::cout << "Ammo Me-Fist U: irrelevant"  << std::endl;
	std::cout << "Ammo Enemy-Fist U: " << FistEU[3] << std::endl;
	std::cout << "FU Fist: " << ArithmeticalFU(FistEU) << std::endl;


}

// Programm ausführen: STRG+F5 oder "Debuggen" > Menü "Ohne Debuggen starten"
// Programm debuggen: F5 oder "Debuggen" > Menü "Debuggen starten"

// Tipps für den Einstieg: 
//   1. Verwenden Sie das Projektmappen-Explorer-Fenster zum Hinzufügen/Verwalten von Dateien.
//   2. Verwenden Sie das Team Explorer-Fenster zum Herstellen einer Verbindung mit der Quellcodeverwaltung.
//   3. Verwenden Sie das Ausgabefenster, um die Buildausgabe und andere Nachrichten anzuzeigen.
//   4. Verwenden Sie das Fenster "Fehlerliste", um Fehler anzuzeigen.
//   5. Wechseln Sie zu "Projekt" > "Neues Element hinzufügen", um neue Codedateien zu erstellen, bzw. zu "Projekt" > "Vorhandenes Element hinzufügen", um dem Projekt vorhandene Codedateien hinzuzufügen.
//   6. Um dieses Projekt später erneut zu öffnen, wechseln Sie zu "Datei" > "Öffnen" > "Projekt", und wählen Sie die SLN-Datei aus.
